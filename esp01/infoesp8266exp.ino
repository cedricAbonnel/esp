ADC_MODE(ADC_VCC);

void setup()
{
  Serial.begin(115200);
}

void loop()
{
  /* ESP8266EX */
  Serial.println("INFO ESP8266EX");
  Serial.printf("Numéro de série de l'ESP8266EX .....: %u\n", ESP.getChipId());
  float frequenceCpu = float(ESP.getCpuFreqMHz());
  Serial.printf("Fréquence du CPU ...................: %4.1f MHz\n", frequenceCpu);
  float vcc = float(ESP.getVcc()) / 1000;
  Serial.printf("Alimentation .......................: %4.2f V\n", vcc);  
  String chaine = ESP.getCoreVersion();
  chaine.replace('_', '.');
  Serial.print("Version du gestionnaire de carte...: ");
  Serial.println(chaine); 
  Serial.printf("Version du SDK .....................: %s\n\n", ESP.getSdkVersion());
  /* Mémoire flash */
  Serial.println("INFO FLASH");
  Serial.printf("Numéro de série du chip ............: %u\n", ESP.getFlashChipId());
  float frequenceFlash = float(ESP.getFlashChipSpeed()) /1000000;
  Serial.printf("Fréquence ..........................: %4.1f MHz\n", frequenceFlash);  
  Serial.printf("capacité effective .................: %u octets\n", ESP.getFlashChipRealSize());
  Serial.printf("capacité paramétrée dans l'EDI .....: %u octets\n", ESP.getFlashChipSize());
  Serial.printf("Taille du sketch ...................: %u octets\n", ESP.getSketchSize());
  Serial.printf("Mémoire disponible .................: %u octets\n\n\n", ESP.getFreeSketchSpace());
  delay(10000);
}
